#include <stdio.h>
#include <stdlib.h>

#define is_digit(c) ((c) >= '0' && (c) <= '9')

int parse_int(const char *string) {
    int ret_value = 0;
    int detected_value = 0;
    int neg = 1;
    if (*string == '-') {
        neg = -1;
        string++;
    }
    while (*string) {
        if is_digit((char) *string) {
            //printf("digit=%c\n", *string);
            detected_value = *string - '0';
            //printf("as int: %d\n", detected_value);
            ret_value = (ret_value * 10) + detected_value;
        }
        else {
            break;
        }
        string++;
    }
    printf("ret value = %d\n", ret_value * neg);
    return ret_value * neg;
}

void main(void) {
    if is_digit('1')
        printf("is digit\n");
    if is_digit('a')
        printf("a is digit!!!\n");
    else
        printf("a is not digit\n\n");
    
    printf("%d\n", atoi("0123"));
    printf("%d\n", atoi("0123.12"));
    printf("%d\n", atoi("1a2b3c"));
    printf("%d\n", atoi("-123"));
    printf("%d\n", atoi("01230"));

    parse_int("1234567890\n");
    parse_int("12345.67890\n");
    parse_int("00012\n");
    parse_int("abc0123\n");
    parse_int("0A1234\n");
    parse_int("-12009\n");
}
