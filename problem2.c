#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define SIZE_PTR ((size_t) sizeof(char *))

/**
 * Validate a given alignment, where it must be power of two
 * (2^n, where 0 < n < 31)
 *
 * Return: int 1 if validated, 0 otherwise
 */
int _validate_alignment(size_t alignment){
    if(alignment > 0x80000000 || alignment < 2){
        printf("Invalid alignment %lu (must be 2^n where 0 < n < 32 [2,4,8,...,%u])\n", alignment, 0x80000000);
        return 0;
    }

    while (((alignment % 2) == 0) && alignment > 1) {
        alignment /= 2;
    }
    if(alignment != 1){
        printf("Alignment not a power of two (must be 2^n where 0 < n < 32)\n");
        return 0;
    }
    return 1;
}

/**
 * Align a provided address by boundary set by alignment
 *
 * Return: void * pointer to new aligned address
 */
void *_align_user_mem(void *mptr, size_t alignment){

    printf("Enter _align_user_mem(%p, alignment=%lu)\n", mptr, alignment);

    if(!_validate_alignment(alignment)) {
        free(mptr);
        return NULL;
    }

    //To get aligned-malloc, from original malloc advance 'SIZE_PTR',
    //then wherever it's at, need to add some more bytes...
    char *user_addr = (char *)mptr + SIZE_PTR;
    printf("  Padded addr: %p\n", user_addr);

    //This requires the calculation of where we are after advancing
    //the number of bytes needed to store the original address.
    //Get the last n signicant bits (where 2^n=N) to get the offset
    //this can be done using N-1 as a mask
    //eg. N = 8 (0000 1000), then N-1 is 7 (0000 0111)
    int offset = (unsigned long int) user_addr & (alignment - 1);
    printf("  Offset: %d\n", offset);

    //Add the bytes needed to reach alignment
    if (offset > 0){
        user_addr += + (alignment - offset);
    }
    
    //now go back ptr_size and write the original malloc address there
    user_addr[- SIZE_PTR] = (unsigned long int) mptr;
    printf("  Aligned addr: %p\n", user_addr);

    printf("Exit _align_user_mem()\n");
    return (void *)user_addr;
}

/**
 * Allocate memory as requested by size and aligned by boundary set by alignment
 *
 * Return: void * pointer to new aligned address
 */
void * aligned_malloc(size_t size, size_t alignment) {
    /*This will need to add enough bytes so that the pointer
    can moved forward to an aligned position (at most needed
    is 'alignment'-1 byte(s)), as well as the
    bytes required to store the original pointer address given
    by internally calling malloc (pointer size is either 32-bit
    or 4 bytes on 32-bit system, or 64-bit = 8 bytes on 64-bit
    system).*/

    //Validate alignment first so as to not allocate at all
    if(!_validate_alignment(alignment)) {
        return NULL;
    }

    //Allocate the maximum size of memory block, which
    //consists of the requested size, generic pointer size, and
    //alignment-1 bytes

    void *malloc_ptr = malloc(size + SIZE_PTR + (alignment - 1));
    printf("System malloc: %p\n", malloc_ptr);

    return (void *)_align_user_mem(malloc_ptr, alignment);

}

/**
 * Get the system-allocated address from an aligned address
 *
 * Return: void * pointer to the original system-allocated address
 */
void * _get_prealigned_addr(void *ptr) {
    if(ptr == NULL) {
        return NULL;
    }
    return (void *)((char *)ptr - SIZE_PTR);
}

/**
 * Free memory block pointed to by an aligned address
 *
 * Return: None
 */
void aligned_free(void * ptr) {
    printf("Enter aligned_free(%p)\n", ptr);
    if(ptr == NULL){
        printf("  Nothing to free\n");
        return;
    }
    void *orig_addr = _get_prealigned_addr(ptr);
    free(orig_addr);
    printf("  Freed %p\n\n", orig_addr);
}


/**********************************************************************
 * TESTS
 *
 * Tests for getting aligned address, validating alignment, and getting
 * the correct address to perform 'aligned_free()'
 **********************************************************************/

int test_bits_and_bytes(){
    printf("\n==== Testing miscellaneous ====\n");
    const unsigned char operand1    = 0x0A; //0000 1010 //10
    const unsigned char operand2    = 0x0C; //0000 1100
    const unsigned char expectedAnd = 0x08; //0000 1000
    assert((operand1 & operand2) == expectedAnd); //8
    //so say from address 10, to go to 16 (align N=8):
    //  0000 1010  //10
    //+ 0000 0110  //+6
    //so need to figure out the '010', that'll solve how much
    //is to be added to make N again (x000) 
    assert((operand1 & 7) == 2); //2

    return 1;
}

int test_validate_alignment(){
    printf("\n==== Test _validate_alignment() ====\n");

    //Invalid alignments [0, 1, >2^31, odd, even-non-2^n]
    printf("\n>>>> Test: alignment=0\n");
    assert(_validate_alignment(0) == 0);

    printf("\n>>>> Test: alignment=1\n");
    assert(_validate_alignment(1) == 0);

    printf("\n>>>> Test: alignment > 2^31\n");
    assert(_validate_alignment(0x80000001) == 0 );

    printf("\n>>>> Test: alignment=9\n");
    assert(_validate_alignment(9) == 0);

    printf("\n>>>> Test: alignment=10\n");
    assert(_validate_alignment(10) == 0);

    //Valid alignments
    printf("\n>>>> Test: alignment=2\n");
    assert(_validate_alignment(2) == 1);

    printf("\n>>>> Test: alignment=64\n");
    assert(_validate_alignment(64) == 1);

    printf("\n>>>> Test: alignment=2^31\n");
    assert(_validate_alignment(0x80000000) == 1 );

    return 8;
}

int test_get_prealigned_addr(){
    void *system_addr, *aligned_addr, *calc_addr;
    int i;
    int alignment = 4;
    //Test _get_prealigned_addr() to ensure freeing of original allocated address
    //Get a system-allocated address addr1, get aligned address addr2 based on addr1,
    //then get the pre-aligned system address addr3 by calling _get_prealign_addr(addr2)
    //then addr1 must equal addr3.
    printf("\n==== Test _get_prealigned_addr() to ensure freeing original address ====\n");
    printf("\n>>>> Test: offset=%d\n", i);
    system_addr = malloc(100);
    printf("System malloc: %p\n", system_addr);
    aligned_addr = _align_user_mem(system_addr, alignment);
    calc_addr = _get_prealigned_addr(aligned_addr);
    assert(calc_addr == system_addr);
    printf("Calculated system malloc: %p\n", calc_addr);
    free(calc_addr);
    printf("Freed %p\n", calc_addr);
    return 1;
}

int test_align_user_mem(){
    void *maddr;
    int i;
    int alignment = 4;
    //Test _align_user_mem() mocking various possible offsets (1-3)
    //for alignment=4
    printf("\n==== Test _align_user_mem() mocking offsets for alignment=4 ====\n");
    for(i = 1; i < alignment; i++){
        printf("\n>>>> Test: offset=%d\n", i);
        maddr = malloc(10);
        printf("System malloc: %p\n", maddr);
        void *aligned_mem = _align_user_mem(maddr+i, alignment);
        assert(((unsigned long int)aligned_mem % alignment) == 0);
        free(maddr);
    }

    //Test _align_user_mem() mocking various possible offsets (1-7)
    //for alignment=2^3=8
    printf("\n==== Test _align_user_mem() mocking offsets for alignment=8 ====\n");
    alignment = 8;
    for(i = 1; i < alignment; i++){
        printf("\n>>>> Test: offset=%d\n", i);
        maddr = malloc(10);
        printf("System malloc: %p\n", maddr);
        void *aligned_mem = _align_user_mem(maddr+i, alignment);
        assert(((unsigned long int)aligned_mem % alignment) == 0);
        free(maddr);
    }
    return 12;
}

int test_aligned_malloc() {
    //Test main aligned_malloc()
    printf("\n==== Testing aligned_malloc ====\n\n");
    printf("\n>>>> Test: aligned_malloc(alignment=4)\n");
    void * amem = aligned_malloc(3, 4);
    assert(amem != NULL);
    assert((unsigned long int)amem % 2 == 0);
    aligned_free(amem);

    printf("\n>>>> Test: aligned_malloc(alignment=8)\n");
    amem = aligned_malloc(3, 8);
    assert(amem != NULL);
    assert((unsigned long int)amem % 2 == 0);
    aligned_free(amem);

    printf("\n>>>> Test: aligned_malloc(alignment=9)\n");
    //Alignment not 2^n
    amem = aligned_malloc(3, 9);
    aligned_free(amem);

    //Alignment larger than 2^31
    printf("\n>>>> Test: aligned_malloc(alignment > 2^31)\n");
    amem = aligned_malloc(3, 0x8000001);
    aligned_free(amem);

    //Alignment less than 2^1
    printf("\n>>>> Test: aligned_malloc(alignment=0)\n");
    assert(aligned_malloc(3, 0) == NULL);

    printf("\n>>>> Test: aligned_malloc(alignment=1)\n");
    assert(aligned_malloc(3, 1) == NULL);
    return 6;
}

void main() {

    int num_tests = 0;
    num_tests += test_bits_and_bytes();
    num_tests += test_validate_alignment();
    num_tests += test_get_prealigned_addr();
    num_tests += test_align_user_mem();
    num_tests += test_aligned_malloc();

    printf("\n\nSuccessfully completed %d test conditions\n", num_tests);
    printf("Done.\n");
}

